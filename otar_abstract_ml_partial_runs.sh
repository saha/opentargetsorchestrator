source /hps/software/users/literature/otar-pipeline/fetcher/common_functions.sh
# Purge existing modules.
module purge

# Load python module
module load python-3.9.10-gcc-9.3.0-i56je3q

# Load python environment for Open Targets ML pipeline.
source /nfs/production/literature/literature_otar/otar_env/bin/activate


TIMESTAMP=$1
TODAY_DATE=$2

getScilitePipelineValue "source_dir" "abstract" "$TODAY_DATE"
inpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "log_dir" "abstract" "$TODAY_DATE"
logpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "summary_dir" "abstract" "$TODAY_DATE"
summarypath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "sent_dir" "abstract" "$TODAY_DATE"
sentpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "plain_dir" "abstract" "$TODAY_DATE"
plainpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "ml_dir" "abstract" "$TODAY_DATE"
mlpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "association_dir" "abstract" "$TODAY_DATE"
associationpath=$SCILITE_PIPELINE_VALUE

if [[ ! -d $sentpath ]];then
        createDirectory "$sentpath"
fi
if [[ ! -d $plainpath ]];then
        createDirectory "$plainpath"
fi
if [[ ! -d $mlpath ]];then
        createDirectory "$mlpath"
fi
if [[ ! -d $associationpath ]];then
        createDirectory "$associationpath"
fi

model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/1604049631/
association_model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/association_model/
OTAR_CODE=/nfs/production/literature/literature_otar/shyama/otar_pipeline
START=0
END=`ls -alh $inpath | grep -o -P "\d+\.abstract\.gz" | grep -oP "\d+" | sort -rn | head -1`
if [[ ! -d $inpath ]];then
	echo $inpath" does not exist"
	PIPELINE_PATH="/hps/software/users/literature/otar-pipeline/fetcher/pipeline.sh"
	ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
	#abstract fetcher pipeline
	sh /hps/software/users/literature/otar-pipeline/fetcher/otar_abstract.sh $TIMESTAMP $TODAY_DATE $PIPELINE_PATH
	echo "Finished fetching."
	# Abstract ML
	bsub -o $ROOT_DIR/$TODAY_DATE.abstract.ml.out -e $ROOT_DIR/$TODAY_DATE.abstract.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_abstract_ml.sh $TIMESTAMP $TODAY_DATE"
	echo "Submitted ML pipeline. Now this script should exit."
	exit 0	
fi

for file_index in $(seq $START $END)
do
        association_file_size=$(wc -c <"$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl")
        ml_file_size=$(wc -c <"$mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl")
        sent_file_size=$(wc -c <"$sentpath/patch-$TIMESTAMP-$file_index.xml")
        echo $file_index
        if [[ -f "$inpath/patch-$TIMESTAMP-$file_index.abstract.gz" ]]; then
                if [[ -f "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl" ]] && [[ $association_file_size -gt $minimumsize ]]; then
                        echo "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl found"
                        echo "Size of the file:"$association_file_size
                elif [[ -f "$mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl" ]] && [[ $ml_file_size -gt $minimumsize ]]; then
                        echo "Running only Association Classification."
                        echo "Size of the ML NER file:"$ml_file_size
                        bsub -J "OTAR_Abstract" -n 4 -q production -M 4000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Abstracts/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
                elif [[ -f "$sentpath/patch-$TIMESTAMP-$file_index.xml" ]] && [[ $sent_file_size -gt $minimumsize ]]; then
                        echo "Running from NER."
                        echo "Size of the sentencised file:"$sent_file_size
                        bsub -J "OTAR_Abstract" -n 4 -q production -M 8000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/OTAR_new_pipeline_cluster_all.py -f $plainpath/patch-$TIMESTAMP-$file_index.xml -o $mlpath/ -m $model -l $summarypath/otar-$TIMESTAMP.tsv -d a;python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Abstracts/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
		else
                        echo "Running Complete pipeline."
                        bsub -J "OTAR_Abstract" -n 4 -q production -M 8000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/Sentenciser.py -f $inpath/patch-$TIMESTAMP-$file_index.abstract.gz -o $sentpath/patch-$TIMESTAMP-$file_index.xml -d a;python $OTAR_CODE/CleanTags.py -f $sentpath/patch-$TIMESTAMP-$file_index.xml -o $plainpath/patch-$TIMESTAMP-$file_index.xml -d a;python $OTAR_CODE/OTAR_new_pipeline_cluster_all.py -f $plainpath/patch-$TIMESTAMP-$file_index.xml -o $mlpath/ -m $model -l $summarypath/otar-$TIMESTAMP.tsv -d a;python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Abstracts/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
                fi
        else
                echo "$inpath/patch-$TIMESTAMP-$file_index.abstract.gz does not exist"
        fi
done
deactivate
