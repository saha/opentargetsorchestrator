ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
start_cond=$(date -d 2022-05-27 +%s)
end_cond=$(date -d 2022-05-29 +%s)
echo $cond
for i in /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/*
do
if [ -d "$i" ]; then
	#echo $i
	date_dir=$(basename $i)
	#echo "date_dir:"$date_dir
	target_date=$( echo ${date_dir} | awk -v FS=_ -v OFS=- '{print $3,$2,$1}')
        #echo "Target_date:"$target_date
        count=1
	target_date_cond=$(date -d "${target_date}" +%s)
	#echo "target_date_cond:"$target_date_cond
        time_stamp=$( echo `date -d "${target_date} - ${count} days" +"%d-%m-%Y"`)
	if [[ $target_date_cond -gt $start_cond ]];then
		if [[ $target_date_cond -lt $end_cond ]];then
			echo "date: "$target_date", prev_day: "$time_stamp
			bsub -o $ROOT_DIR/$date_dir.abstract.ml.out -e $ROOT_DIR/$date_dir.abstract.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_abstract_ml_partial_runs.sh $time_stamp $date_dir"	
		else
			echo $end_cond" onwards"
		fi
	fi
fi
done
