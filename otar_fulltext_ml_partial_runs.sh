source /hps/software/users/literature/otar-pipeline/fetcher/common_functions.sh
# Purge existing modules.
module purge

# Load python module
module load python-3.9.10-gcc-9.3.0-i56je3q

# Load python environment for Open Targets ML pipeline.
source /nfs/production/literature/literature_otar/otar_env/bin/activate

TIMESTAMP=$1
TODAY_DATE=$2


getScilitePipelineValue "source_dir" "fulltext" "$TODAY_DATE"
inpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "log_dir" "fulltext" "$TODAY_DATE"
logpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "summary_dir" "fulltext" "$TODAY_DATE"
summarypath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "sent_dir" "fulltext" "$TODAY_DATE"
sentpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "plain_dir" "fulltext" "$TODAY_DATE"
plainpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "section_dir" "fulltext" "$TODAY_DATE"
sectionpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "ml_dir" "fulltext" "$TODAY_DATE"
mlpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "association_dir" "fulltext" "$TODAY_DATE"
associationpath=$SCILITE_PIPELINE_VALUE

if [[ ! -d $sentpath ]];then
	createDirectory "$sentpath"
fi
if [[ ! -d $plainpath ]];then
	createDirectory "$plainpath"
fi
if [[ ! -d $sectionpath ]];then
	createDirectory "$sectionpath"
fi
if [[ ! -d $mlpath ]];then
	createDirectory "$mlpath"
fi
if [[ ! -d $associationpath ]];then
	createDirectory "$associationpath"
fi

model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/1604049631/
association_model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/association_model/
OTAR_CODE=/nfs/production/literature/literature_otar/shyama/otar_pipeline
if [[ ! -d $inpath ]];then
	echo $inpath" does not exist"
	PIPELINE_PATH="/hps/software/users/literature/otar-pipeline/fetcher/pipeline.sh"
	ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
	#fulltext fetcher pipeline
	sh /hps/software/users/literature/otar-pipeline/fetcher/otar_fulltext.sh $TIMESTAMP $TODAY_DATE $PIPELINE_PATH
	echo "Finished fetching."
	# full-text ML
	bsub -o $ROOT_DIR/$TODAY_DATE.fulltext.ml.out -e $ROOT_DIR/$TODAY_DATE.fulltext.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_fulltext_ml.sh $TIMESTAMP $TODAY_DATE"
	echo "Submitted ML pipeline. Now this script should exit."
	exit 0	
fi
START=0
END=`ls -alh $inpath | grep -o -P "\d+\.xml\.gz" | grep -oP "\d+" | sort -rn | head -1`
minimumsize=4000
ner_job_memory=8000
association_job_memory=4000
for file_index in $(seq $START $END)
do
	# Following code checks for error msg in the log file. e.g. if the ML pipeline was terminated due to memory issue. If we find TERM_MEMLIMIT, 
	# assumption is the pipeline failed from the ML NER. Hence even if we file jsonl file and the ner JSON file is larger than 4k, the pipeline
	# should run from the NER.
	error_msg=`tail -n 26 $logpath/patch-$TIMESTAMP-$file_index.ml.out | grep -o "TERM_MEMLIMIT"`
	association_file_size=$(wc -c <"$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl")
	ml_file_size=$(wc -c <"$mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl")
	sent_file_size=$(wc -c <"$sentpath/patch-$TIMESTAMP-$file_index.xml")
        echo $file_index
	if [[ -f "$inpath/patch-$TIMESTAMP-$file_index.xml.gz" ]]; then
                if [[ -f "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl" ]] && [[ $association_file_size -gt $minimumsize ]] && [[ $error_msg = "" ]]; then
                        echo "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl found"
			echo "Size of the file:"$association_file_size
                elif [[ -f "$mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl" ]] && [[ $ml_file_size -gt $minimumsize ]] && [[ $error_msg = "" ]]; then
			echo "Running only Association Classification."
			echo "Size of the ML NER file:"$ml_file_size
			bsub -J "OTAR_Fulltext" -n 4 -q production -M 4000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Full-text/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
		elif [[ -f "$sentpath/patch-$TIMESTAMP-$file_index.xml" ]] && [[ $sent_file_size -gt $minimumsize ]]; then
			echo "Running from NER."
			echo "Size of the sentencised file:"$sent_file_size
			if [[ $error_msg = "" ]]; then
				memory=8000
			else
				memory=16000
			fi
				bsub -J "OTAR_Fulltext" -n 4 -q production -M $memory -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/OTAR_new_pipeline_cluster_all.py -f $sectionpath/patch-$TIMESTAMP-$file_index.xml -o $mlpath/ -m $model -l $summarypath/otar-$TIMESTAMP.tsv -d f;python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Full-text/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
		else
			echo "Running Complete pipeline."
                        bsub -J "OTAR_Fulltext" -n 4 -q production -M 8000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/Sentenciser.py -f $inpath/patch-$TIMESTAMP-$file_index.xml.gz -o $sentpath/patch-$TIMESTAMP-$file_index.xml -d f;python $OTAR_CODE/CleanTags.py -f $sentpath/patch-$TIMESTAMP-$file_index.xml -o $plainpath/patch-$TIMESTAMP-$file_index.xml -d f; python $OTAR_CODE/OTAR_new_pipeline_section_tagger.py -f $plainpath/patch-$TIMESTAMP-$file_index.xml -o $sectionpath/;python $OTAR_CODE/OTAR_new_pipeline_cluster_all.py -f $sectionpath/patch-$TIMESTAMP-$file_index.xml -o $mlpath/ -m $model -l $summarypath/otar-$TIMESTAMP.tsv -d f;python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Full-text/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
                fi
        else
                echo "$inpath/patch-$TIMESTAMP-$file_index.xml.gz does not exist"
        fi
done

deactivate

