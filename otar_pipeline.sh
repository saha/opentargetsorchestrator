ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
cd $ROOT_DIR
pwd

TIMESTAMP=$1
TODAY_DATE=$2
PARAM_PATH=$3
#sh /hps/software/users/literature/commons/remove_old_data.sh /hps/nobackup/literature/otar-pipeline/logs 7 f
#sh /hps/software/users/literature/commons/remove_old_data.sh /hps/nobackup/literature/otar-pipeline/daily_pipeline_api 7 d

PIPELINE_PATH="/hps/software/users/literature/otar-pipeline/fetcher/pipeline.sh"

#fulltext pipeline
sh /hps/software/users/literature/otar-pipeline/fetcher/otar_fulltext.sh $TIMESTAMP $TODAY_DATE $PIPELINE_PATH $PARAM_PATH


echo "Finished fulltext pipeline, about to start abstract pipeline"

#abstract pipeline
sh /hps/software/users/literature/otar-pipeline/fetcher/otar_abstract.sh $TIMESTAMP $TODAY_DATE $PIPELINE_PATH $PARAM_PATH

echo "Finished abstract pipeline, about to start fulltext ml pipeline"

# OTAR NER and association pipeline
# full-text
bsub -o $ROOT_DIR/$TODAY_DATE.fulltext.ml.out -e $ROOT_DIR/$TODAY_DATE.fulltext.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_fulltext_ml.sh $TIMESTAMP $TODAY_DATE"
echo "Submitted full-text ML pipeline!"

# abstracts
bsub -o $ROOT_DIR/$TODAY_DATE.abstract.ml.out -e $ROOT_DIR/$TODAY_DATE.abstract.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_abstract_ml.sh $TIMESTAMP $TODAY_DATE"
echo "Submitted abstract ML pipeline!"

