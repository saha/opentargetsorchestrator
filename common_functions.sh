createDirectory(){
    if [ -d "$1" ]
    then
      rm -R "$1"
      echo "Removing dir $1"
    fi

    if [ ! -d "$1" ]
    then
      mkdir -p "$1"
      echo "Created dir $1"
    fi
}

initiateVariables(){
        echo "Initializing variables"
	MAIL_RECIPIENTS="lit-dev@ebi.ac.uk"
}


getRootDirectory(){
	 if [ "$1" = "fulltext" ]
         then
                SCILITE_PIPELINE_ROOT_DIRECTORY="/hps/nobackup/literature/otar-pipeline/daily_pipeline_api/$2/fulltext"
         else
                SCILITE_PIPELINE_ROOT_DIRECTORY="/hps/nobackup/literature/otar-pipeline/daily_pipeline_api/$2/abstract"
         fi
}


getScilitePipelineValue(){
        #echo "first parameter $1 $2 $3"
        getRootDirectory "$2" "$3"
	if [ "$1" = "root_dir" ]
     	then
      		SCILITE_PIPELINE_VALUE=$SCILITE_PIPELINE_ROOT_DIRECTORY
        elif [ "$1" = "log_dir" ]
        then
               	SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/log"
        elif [ "$1" = "summary_dir" ]
        then
               	SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/summary"
        elif [ "$1" = "source_dir" ]
        then
               	SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/source"
	elif [ "$1" = "sent_dir" ]
	then
		SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/sents"
	elif [ "$1" = "plain_dir" ]
	then
		SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/plain"
	elif [ "$1" = "section_dir" ]
	then
		SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/section"
	elif [ "$1" = "ml_dir" ]
	then
		SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/ml"
	elif [ "$1" = "association_dir" ]
	then
		SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/association"
	elif [ "$1" = "log_file" ]
        then
               	SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/log/log.txt"
        elif [ "$1" = "error_file" ]
        then
               	SCILITE_PIPELINE_VALUE="$SCILITE_PIPELINE_ROOT_DIRECTORY/log/error.txt"
	elif [ "$1" = "java_classpath" ]
        then
                folder_java="/hps/software/users/literature/textmining/scilite_loader"
		lib_java="$folder_java/lib"
		main_jar=$folder_java/scilite_loader.jar
		OJDBC_DRIVER=/hps/software/users/literature/commons/jars/jar_db/ojdbc8.jar
		CP_JAVA=".:$main_jar:$OJDBC_DRIVER:$lib_java/*"
                SCILITE_PIPELINE_VALUE=$CP_JAVA
	fi
}
