ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
#start_cond=$(date -d 2022-03-22 +%s)
#end_cond=$(date -d 2022-09-01 +%s)
#daily_pipeline='/hps/nobackup/literature/otar-pipeline/daily_pipeline_api'
#echo $cond
#for i in $daily_pipeline/*
#do
#if [ -d "$i" ]; then
#	#echo $i
#	date_dir=$(basename $i)
#	#echo "date_dir:"$date_dir
#	target_date=$( echo ${date_dir} | awk -v FS=_ -v OFS=- '{print $3,$2,$1}')
#        #echo "Target_date:"$target_date
#        count=1
#	target_date_cond=$(date -d "${target_date}" +%s)
#        time_stamp=$( echo `date -d "${target_date} - ${count} days" +"%d-%m-%Y"`)
#	if [[ $target_date_cond -gt $start_cond ]];then
#		if [[ $target_date_cond -lt $end_cond ]];then
#			echo "Path: "$i/fulltext/association
#			START=0
#			END=`ls -alh $i/fulltext/association | grep -o -P "\d+\.jsonl" | grep -oP "\d+" | sort -rn | head -1`
#			echo "date: "$target_date", prev_day: "$time_stamp
#			echo "start: "$START" end: "$END
#			for file_index in $(seq $START $END)
#			do
#				echo $i/fulltext/association/NMP_patch-$time_stamp-$file_index.jsonl
#				sed -i 's/}{\"pmcid/}\n{\"pmcid/g' $i/fulltext/association/NMP_patch-$time_stamp-$file_index.jsonl
#				gsutil cp $i/fulltext/association/NMP_patch-$time_stamp-$file_index.jsonl gs://otar025-epmc/Full-text/$date_dir/NMP_patch-$time_stamp-$file_index.jsonl	
#			done	
#		else
#			echo $end_cond" onwards"
#		fi
#	fi
#fi
#done

start_cond=$(date -d 2022-05-27 +%s)
end_cond=$(date -d 2022-09-01 +%s)
daily_pipeline='/hps/nobackup/literature/otar-pipeline/daily_pipeline_api'
for i in $daily_pipeline/*
do
if [ -d "$i" ]; then
        #echo $i
        date_dir=$(basename $i)
        #echo "date_dir:"$date_dir
        target_date=$( echo ${date_dir} | awk -v FS=_ -v OFS=- '{print $3,$2,$1}')
        #echo "Target_date:"$target_date
        count=1
        target_date_cond=$(date -d "${target_date}" +%s)
        time_stamp=$( echo `date -d "${target_date} - ${count} days" +"%d-%m-%Y"`)
        if [[ $target_date_cond -gt $start_cond ]];then
                if [[ $target_date_cond -lt $end_cond ]];then
                        echo "Path: "$i/abstract/association
                        START=0
                        END=`ls -alh $i/abstract/association | grep -o -P "\d+\.jsonl" | grep -oP "\d+" | sort -rn | head -1`
                        echo "date: "$target_date", prev_day: "$time_stamp
                        echo "start: "$START" end: "$END
                        for file_index in $(seq $START $END)
                        do
                                echo $i/abstract/association/NMP_patch-$time_stamp-$file_index.jsonl
                                sed -i 's/}{\"pmcid/}\n{\"pmcid/g' $i/abstract/association/NMP_patch-$time_stamp-$file_index.jsonl
                                gsutil cp $i/abstract/association/NMP_patch-$time_stamp-$file_index.jsonl gs://otar025-epmc/Abstracts/$date_dir/NMP_patch-$time_stamp-$file_index.jsonl
                        done
                else
                        echo $end_cond" onwards"
		fi
	fi
fi
done
