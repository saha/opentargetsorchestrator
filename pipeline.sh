source /hps/software/users/literature/commons/scripts/SetJava11ClassPath.sh

OJDBC_DRIVER=/hps/software/users/literature/commons/jars/jar_db/ojdbc8.jar
UKPMC=/hps/software/users/literature/otar-pipeline/fetcher/lib

OTHERS=$OJDBC_DRIVER:$UKPMC/pipeline.jar:$UKPMC/ebitmjimenotools.jar:$UKPMC/monq.jar:$UKPMC/commons-lang-2.4.jar:$UKPMC/commons-io-2.0.1.jar:$UKPMC/jopt-simple-3.2.jar:$UKPMC/jackson-core-2.11.0.rc1.jar:$UKPMC/jackson-databind-2.11.0.rc1.jar:$UKPMC/jackson-annotations-2.11.0.rc1.jar
STDERR=$1

FETCH_FULLTEXT="java -XX:+UseSerialGC -cp $OTHERS -Doracle.net.tns_admin=/hps/software/dbtools/oracle/client/tnsnames ebi.ukpmc.pipeline.fetch.OTARFulltextFetcher "
FETCH_ABSTRACT="$JAVA_HOME/bin/java -XX:+UseSerialGC -cp $OTHERS -Doracle.net.tns_admin=/hps/software/dbtools/oracle/client/tnsnames ebi.ukpmc.pipeline.fetch.abstracts.OTARAbstractFetcher"

# fetch
echo "$FETCH_FULLTEXT --dataDir $4 --timestamp $5 --maxDocs $6 --summaryDir $7 --dbUserCDB $9 --dbPwdCDB ${10} --dbUrlCDB ${11} --dbSchemaCDB ${12} --domainAPI ${13} $8 "
if [ "$2" = "fulltext" ] && [ "$3" = "fetch" ]; then

  $FETCH_FULLTEXT --dataDir $4 --timestamp $5 --maxDocs $6 --summaryDir $7 --dbUserCDB $9 --dbPwdCDB ${10} --dbUrlCDB ${11} --dbSchemaCDB ${12} --domainAPI ${13} 1>> $8 2>> $STDERR
elif [ "$2" = "abstract" ] && [ "$3" = "fetch" ]; then
  $FETCH_ABSTRACT --dataDir $4 --timestampStart $5 --maxDocs $6 --summaryDir $7  --dbUserCDB $9 --dbPwdCDB ${10} --dbUrlCDB ${11} --dbSchemaCDB ${12}  1>> ${8} 2>> $STDERR
fi




