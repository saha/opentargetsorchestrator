source /hps/software/users/literature/otar-pipeline/fetcher/common_functions.sh
source /hps/software/users/literature/commons/scripts/SetJava8ClassPath.sh

initiateVariables

TIMESTAMP=$1
TODAY_DATE=$2
PIPELINE_PATH=$3
DB_PARAM_PATH=$4

declare -A DB_PARAMS

while IFS=" " read line val
do
   echo $line : $val;
   DB_PARAMS[$line]=$val
done < $DB_PARAM_PATH

MAX_DOCS=20000

getScilitePipelineValue "log_dir" "abstract" "$TODAY_DATE"
LOG_DIR=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "source_dir" "abstract" "$TODAY_DATE"
SRC_DIR=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "log_file" "abstract" "$TODAY_DATE"
LOG_FILE=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "error_file" "abstract" "$TODAY_DATE"
ERR_FILE=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "summary_dir" "abstract" "$TODAY_DATE"
SUMMARY_DIR=$SCILITE_PIPELINE_VALUE

createDirectory "$LOG_DIR"
createDirectory "$SRC_DIR"
createDirectory "$SUMMARY_DIR"

echo "Timestamp $TIMESTAMP" >> $LOG_FILE
echo "Today $TODAY_DATE" >> $LOG_FILE
echo "Pipeline path abstract $PIPELINE_PATH" >> $LOG_FILE

echo "MAIN ABSTRACT PIPELINE LOG_DIR $LOG_DIR SUMMARY_DIR $SUMMARY_DIR LOG $LOG_FILE ERR $ERR_FILE  SRC DIR $SRC_DIR" >> $LOG_FILE


#fetching  abstract data into the source folder
sh $PIPELINE_PATH $ERR_FILE abstract fetch $SRC_DIR $TIMESTAMP $MAX_DOCS $SUMMARY_DIR $LOG_FILE $DB_PARAMS['USERNAME_DB_CDB'] $DB_PARAMS['PASSWORD_DB_CDB'] $DB_PARAMS['URL_DB_CDB'] $DB_PARAMS['SCHEMA_DB_CDB']



