# OpenTargetsOrchestrator

Scripts of this repo facilitates the incremental/daily data processing for the Open Targets platform. Broadly we process two types of articles, abstracts and full-text, as part of this project. Abstracts two different sources.
- Medline/PubMed
- Pre-prints

Abstracts and full-text articles are fetched from Europe PMC database on a daily basis in two different XML formats. Please check the example XML file from the example folder of https://gitlab.ebi.ac.uk/saha/opentargets repo. The daily fetcher stores the articles under current date and the articles are from previous day. The file system is accessible from the EBI Codon cluster and the path is /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/DD_MM_YYY. Each os these folders will contain two folders i) abstract, ii) fulltext. The articles are stored in a subfolder named 'source' and the log files goes into the log folder.

These scripts also create five folders for the abstracts and six folders for the full-text articles to store intermediate files generated by the different components of the Open Targets ML pipeline. The fetcher script saves a list of abstract ids and the source of the abstract, i.e. pre-print and medline in a file named abstracts_list.csv [the file is tab delimited]. For the fulltext data the file is called pmcids_list.csv. There is an extra PMID to PMCID mapping file generated for the full-text articles [otar-DD-MM-YYYY.tsv , here the DD = Folder day - 1 day]. This file is required by the full-text Open Targets ML pipeline. 

The folder structure for abstract is
/hps/nobackup/literature/otar-pipeline/daily_pipeline_api/DD_MM_YYY/abstract <br>
|__ source <br>
|__ log <br>
|__ summary <br>
|__ sents <br>
|__ plain <br>
|__ ml <br>
|__ association

The folder structure for full-text is
/hps/nobackup/literature/otar-pipeline/daily_pipeline_api/DD_MM_YYY/fulltext <br>
|__ source <br>
|__ log <br>
|__ summary <br>
|__ sents <br>
|__ plain <br>
|__ section <br>
|__ ml <br>
|__ association

The Open Targets Literature mining pipeline starts with a bulk article data submission. All the abstracts and full-text articles up-until a given date are fetched from the database to create the bulk article dataset. From that day onwards, the article fetcher cron job fetch abstracts and full-text articles from the database and save them in appropriate folders in zipped files. Currently, full-text bulk data was collected on 23_03_2022 and abstract bulk data is untill 27_05_2022. The data files get a sequential number and each file contains XX articles. Bulk data files are named as patch-total-X.xml.gz where X is a number starting from 0.

