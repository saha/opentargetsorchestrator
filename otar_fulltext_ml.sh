source /hps/software/users/literature/otar-pipeline/fetcher/common_functions.sh
# Purge existing modules.
module purge

# Load python module
module load python-3.9.10-gcc-9.3.0-i56je3q

# Load python environment for Open Targets ML pipeline.
source /nfs/production/literature/literature_otar/otar_env/bin/activate

TIMESTAMP=$1
TODAY_DATE=$2


getScilitePipelineValue "source_dir" "fulltext" "$TODAY_DATE"
inpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "log_dir" "fulltext" "$TODAY_DATE"
logpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "summary_dir" "fulltext" "$TODAY_DATE"
summarypath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "sent_dir" "fulltext" "$TODAY_DATE"
sentpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "plain_dir" "fulltext" "$TODAY_DATE"
plainpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "section_dir" "fulltext" "$TODAY_DATE"
sectionpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "ml_dir" "fulltext" "$TODAY_DATE"
mlpath=$SCILITE_PIPELINE_VALUE

getScilitePipelineValue "association_dir" "fulltext" "$TODAY_DATE"
associationpath=$SCILITE_PIPELINE_VALUE

createDirectory "$sentpath"
createDirectory "$plainpath"
createDirectory "$sectionpath"
createDirectory "$mlpath"
createDirectory "$associationpath"

model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/1604049631/
association_model=/nfs/production/literature/literature_otar/shyama/otar_pipeline/association_model/
OTAR_CODE=/nfs/production/literature/literature_otar/shyama/otar_pipeline
START=0
END=`ls -alh $inpath | grep -o -P "\d+\.xml\.gz" | grep -oP "\d+" | sort -rn | head -1`
for file_index in $(seq $START $END)
do
        echo $file_index
        if [[ -f "$inpath/patch-$TIMESTAMP-$file_index.xml.gz" ]]; then
                if [[ -f "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl" ]]; then
                        echo "$associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl found"
                else
                        bsub -J "OTAR_Fulltext" -n 4 -q production -M 8000 -o $logpath/patch-$TIMESTAMP-$file_index.ml.out -e $logpath/patch-$TIMESTAMP-$file_index.ml.err "python $OTAR_CODE/Sentenciser.py -f $inpath/patch-$TIMESTAMP-$file_index.xml.gz -o $sentpath/patch-$TIMESTAMP-$file_index.xml -d f;python $OTAR_CODE/CleanTags.py -f $sentpath/patch-$TIMESTAMP-$file_index.xml -o $plainpath/patch-$TIMESTAMP-$file_index.xml -d f; python $OTAR_CODE/OTAR_new_pipeline_section_tagger.py -f $plainpath/patch-$TIMESTAMP-$file_index.xml -o $sectionpath/;python $OTAR_CODE/OTAR_new_pipeline_cluster_all.py -f $sectionpath/patch-$TIMESTAMP-$file_index.xml -o $mlpath/ -m $model -l $summarypath/otar-$TIMESTAMP.tsv -d f;python $OTAR_CODE/otar_association_annotation.py -f $mlpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -o $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl -m $association_model;gsutil cp $associationpath/NMP_patch-$TIMESTAMP-$file_index.jsonl gs://otar025-epmc/Full-text/$TODAY_DATE/NMP_patch-$TIMESTAMP-$file_index.jsonl"
                fi
        else
                echo "$inpath/patch-$TIMESTAMP-$file_index.xml.gz does not exist"
        fi
done

deactivate

