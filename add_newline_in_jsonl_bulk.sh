daily_pipeline='/hps/nobackup/literature/otar-pipeline/daily_pipeline_api'
echo $cond
#i=$daily_pipeline/23_03_2022
i=$daily_pipeline/27_05_2022
date_dir=$(basename $i)
#mkdir /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/sents
#mkdir /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/plain
#mkdir /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/ml
#mkdir /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/association
#cp /hps/nobackup/literature/literature_otar/medline/v.2022.05/sents/*.xml /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/28_05_2022/abstract/sents/
#cp /hps/nobackup/literature/literature_otar/medline/v.2022.05/plain/*.xml /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/plain/
#cp /hps/nobackup/literature/literature_otar/medline/v.2022.05/ML/*jsonl /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/ml/
#cp /hps/nobackup/literature/literature_otar/medline/v.2022.05/Association/*.jsonl /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/27_05_2022/abstract/association/
START=1			
#END=`ls -alh $i/fulltext/association | grep -o -P "\d+\.jsonl" | grep -oP "\d+" | sort -rn | head -1`
END=`ls -alh $i/abstract/association | grep -o -P "\d+\.jsonl" | grep -oP "\d+" | sort -rn | head -1`
echo "start: "$START" end: "$END
for file_index in $(seq $START $END)
do
	echo $i/abstract/association/NMP_patch-total-$file_index.jsonl
	sed -i 's/}{\"pmcid/}\n{\"pmcid/g' $i/abstract/association/NMP_patch-total-$file_index.jsonl
	gsutil cp $i/abstract/association/NMP_patch-total-$file_index.jsonl gs://otar025-epmc/Abstracts/$date_dir/NMP_patch-total-$file_index.jsonl	
	#echo $date_dir
done	
