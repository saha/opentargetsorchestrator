# This code runs the otar fulltext pipeline for a given date range.
ROOT_DIR="/hps/nobackup/literature/otar-pipeline/logs/"
start_cond=$(date -d 2022-09-01 +%s)
end_cond=$(date -d 2022-09-06 +%s)
echo $cond
for i in /hps/nobackup/literature/otar-pipeline/daily_pipeline_api/*
do
if [ -d "$i" ]; then
	date_dir=$(basename $i)
	target_date=$( echo ${date_dir} | awk -v FS=_ -v OFS=- '{print $3,$2,$1}')
        count=1
	target_date_cond=$(date -d "${target_date}" +%s)
        time_stamp=$( echo `date -d "${target_date} - ${count} days" +"%d-%m-%Y"`)
	if [[ $date_dir = '23_03_2022' ]];then
		time_stamp='total'
	fi
	if [[ $target_date_cond -gt $start_cond ]];then
		if [[ $target_date_cond -lt $end_cond ]];then
			echo "date_dir:"$date_dir
			echo "date: "$target_date", prev_day: "$time_stamp
			bsub -o $ROOT_DIR/$date_dir.fulltext.ml.out -e $ROOT_DIR/$date_dir.fulltext.ml.err "sh /hps/software/users/literature/otar-pipeline/fetcher/otar_fulltext_ml_partial_runs.sh $time_stamp $date_dir"	
		fi
	fi
fi
done
